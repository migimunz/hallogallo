#include "notes.h"

struct BeeperNote;
typedef int(*ModFT)(BeeperNote&, LengthT &t);

struct BeeperNote
{
  int freq;
  LengthT duration;
  ModFT modF;

  BeeperNote();
  BeeperNote(int freq, LengthT duration = QUARTER, ModFT modF = NULL);
  void play(LengthT &t);
};

struct BeeperPattern
{
  BeeperNote *notes;
  int size, noteIdx;
  BeeperPattern *nextPattern;
  LengthT patternT, noteT, beatDuration;

  BeeperPattern();
  BeeperPattern(BeeperNote *notes, int size, LengthT beatDuration = QUARTER, BeeperPattern *nextPattern = NULL);
  BeeperPattern* play();
  inline void reset();
  BeeperNote *currentNote();
};