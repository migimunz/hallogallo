#include "beeper.h"
#include "notes.h"
#include "toneAC.h"

BeeperNote::BeeperNote()
  : freq(nC2)
  , duration(QUARTER)
  , modF(NULL)
{
}

BeeperNote::BeeperNote(int freq, LengthT duration, ModFT modF)
  : freq(freq)
  , duration(duration)
  , modF(modF)
{
}

void BeeperNote::play(LengthT &t)
{
  if(freq == 0) // Silent note
  {
    noToneAC();
    return;
  }
  if(modF == NULL) // just play note
  {
    toneAC(freq);
  }
  else
  {
    toneAC(modF(*this, t));
  }
}


BeeperPattern::BeeperPattern()
  : notes(NULL)
  , size(0)
  , noteIdx(0)
  , nextPattern(NULL)
  , patternT(0)
  , noteT(0)
  , beatDuration(QUARTER)
{
}

BeeperPattern::BeeperPattern(BeeperNote *notes, int size, LengthT beatDuration, BeeperPattern *nextPattern)
  : notes(notes)
  , size(size)
  , noteIdx(0)
  , nextPattern(nextPattern)
  , patternT(0)
  , noteT(0)
  , beatDuration(beatDuration)
{
}

void BeeperPattern::reset()
{
  patternT = noteT = noteIdx = 0;
}

BeeperPattern* BeeperPattern::play()
{
  if(patternT == 0)
  {
    patternT = millis();
  }
  LengthT nowT = millis();
  noteT += (nowT - patternT);
  patternT = nowT;

  BeeperNote *note = &notes[noteIdx];
  if(noteT >= note->duration || noteT >= beatDuration)
  {
    if(noteT >= beatDuration)
    {
      noteIdx++;
      if(noteIdx == size)
      {
        reset();
        return nextPattern == NULL ? this : nextPattern;
      }
      else
      {
        note = &notes[noteIdx];
        noteT = noteT - beatDuration;
      }
    }
    else
    {
      noToneAC();
    }
  }
  else
  {
    note->play(noteT);
  }

  return this; // return next if done or this if null
  //TODO: On end, reset times
}

BeeperNote* BeeperPattern::currentNote()
{
  return &notes[noteIdx];
}