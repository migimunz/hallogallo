// hallogallo.ino
#define BPM 120
#include "beeper.h"
#include "toneAC.h"
#include "notes.h"

int linear(BeeperNote &n, LengthT &t)
{
  return n.freq + (nA2 * t) / QUARTER;
}

int quadratic(BeeperNote &n, LengthT &t)
{
  LengthT x = t - n.duration / 2;

  return n.freq - (-x * x) + nA0;
}

BeeperNote notes[] = {
  BeeperNote(nC3, WHOLE, &quadratic),
  BeeperNote(nA3, WHOLE, &quadratic),
  BeeperNote(nF3, WHOLE, &quadratic),
  BeeperNote(nC3, WHOLE, &quadratic),
  BeeperNote(0),
  BeeperNote(nA4),
  BeeperNote(nE4),
  BeeperNote(nF4),
  BeeperNote(nC4),
  BeeperNote(nA4, WHOLE),
  BeeperNote(nA4, WHOLE),
  BeeperNote(0),
};

LengthT time = 0;
LengthT noteTime = 0;

BeeperPattern p1;

void setup() 
{
  Serial.begin(9600);
  p1.notes = notes;
  p1.size = sizeof(notes)/sizeof(BeeperNote);
  p1.beatDuration = QUARTER;
}


void loop() 
{
  BeeperPattern* pat = &p1;
  pat = pat->play();
  // Serial.print("noteIdx: ");
  // Serial.print(pat->noteIdx);
  // Serial.print(" of ");
  // Serial.print(pat->size);
  // Serial.print("\n");
}

