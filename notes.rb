
notes = []

File.open("notes", "r") do |f|
  f.each_line do |line|
    note, freq = line.split
    notes << [note.gsub(/\/.*/, '').gsub("#", "S"), freq]
  end
end

File.open("notes.h", "w") do |f|
  f.puts '#ifndef NOTES_H'
  f.puts '#define NOTES_H'
  notes.each do |note|
    f.puts "#define n#{note[0]} #{note[1].to_f.round}"
  end
  f.puts "#endif"
end